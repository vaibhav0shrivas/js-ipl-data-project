// Find the number of times each team won the toss and also won the match

function teamWonTossAndGame(matches) {
  let result = {};
  if (matches === undefined || Array.isArray(matches) == false) {
    return {};
  }

  result = matches.reduce((accumulator, match) => {
    if (match.season !== undefined && match["toss_winner"] == match["winner"]) {
      if (match["winner"] in accumulator) {
        accumulator[match["winner"]] += 1;
      } else {
        accumulator[match["winner"]] = 1;
      }
    }
    return accumulator;
  }, {});
  return result;
}

module.exports = teamWonTossAndGame;
