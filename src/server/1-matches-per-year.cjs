// Number of matches played per year for all the years in IPL.

function matchesPerYear(matches) {
  let result = {};
  if (matches === undefined || Array.isArray(matches) == false) {
    return {};
  }

  result = matches.reduce((accumulator, match) => {
    if (match.season !== undefined) {
      if (match["season"] in accumulator) {
        accumulator[match["season"]] += 1;
      } else {
        accumulator[match["season"]] = 1;
      }
    }
    return accumulator;
  }, {});
  return result;
}

module.exports = matchesPerYear;
