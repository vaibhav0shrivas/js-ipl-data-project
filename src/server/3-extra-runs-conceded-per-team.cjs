// Extra runs conceded per team in the year 2016

function extraRunsPerTeaminYear(matches, deliveries, year) {
  if (
    matches === undefined ||
    Array.isArray(matches) == false ||
    deliveries === undefined ||
    Array.isArray(deliveries) == false ||
    Number.isInteger(year) == false
  ) {
    return {};
  }

  let idOfMatchesinYear = matches.reduce((accumulator, match) => {
    //store match id for all the 2016(provided year) matches
    if (match["season"] == year) {
      accumulator.push(match["id"]);
    }
    return accumulator;
  }, []);

  let result = deliveries.reduce((accumulator, delivery) => {
    if (
      idOfMatchesinYear.includes(delivery["match_id"]) &&
      Number(delivery["extra_runs"]) > 0
    ) {
      if (delivery["bowling_team"] in accumulator) {
        accumulator[delivery["bowling_team"]] += Number(delivery["extra_runs"]);
      } else {
        accumulator[delivery["bowling_team"]] = Number(delivery["extra_runs"]);
      }
    }
    return accumulator;
  }, {});

  return result;
}

module.exports = extraRunsPerTeaminYear;
