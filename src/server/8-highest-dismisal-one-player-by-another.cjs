// Find the highest number of times one player has been dismissed by another player

function highestDismisal(deliveries) {
  if (deliveries === undefined || Array.isArray(deliveries) == false) {
    return {};
  }

  let dismisalStats = deliveries.reduce((accumulator, delivery) => {
    if (
      delivery["player_dismissed"] !== undefined &&
      delivery["player_dismissed"] !== ""
    ) {
      let key = delivery["player_dismissed"].concat(
        "-dismissed by-",
        delivery["bowler"]
      );

      if (key in accumulator == false) {
        accumulator[key] = 0;
      }

      accumulator[key]++;
    }
    return accumulator;
  }, {});

  let sortedDismisalStats = Object.entries(dismisalStats);
  sortedDismisalStats.sort((dismissalEntryA, dismissalEntryB) => {
    return dismissalEntryB[1] - dismissalEntryA[1];
  });

  return sortedDismisalStats[0];
}

module.exports = highestDismisal;
