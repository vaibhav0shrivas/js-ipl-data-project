//Find a player who has won the highest number of Player of the Match awards for each season

function highestPlayerOfMatchEachSeason(matches) {
  if (matches === undefined || Array.isArray(matches) == false) {
    return {};
  }

  let perSeasonPlayerOfMatchCount = matches.reduce((accumulator, match) => {
    if (match["season"] !== undefined) {
      if (match["season"] in accumulator == false) {
        accumulator[match["season"]] = {};
      }
      if (match["player_of_match"] in accumulator[match["season"]] == false) {
        accumulator[match["season"]][match["player_of_match"]] = 1;
      } else {
        accumulator[match["season"]][match["player_of_match"]] += 1;
      }
    }
    return accumulator;
  }, {});

  let result = Object.entries(perSeasonPlayerOfMatchCount).map(
    (seasonEntry) => {
      let playerlist = Object.entries(seasonEntry[1]).sort(
        //seasonEntry[1] holds the object with playername:player stats as fields
        (playerentryA, playerentryB) => {
          //playerentry[1] holds the object with player stats as fields
          if (playerentryA[1] > playerentryB[1]) {
            return -1;
          } else {
            return 1;
          }
        }
      );
      return [seasonEntry[0], playerlist[0]];
    }
  );
  result = Object.fromEntries(result);
  return result;
}

module.exports = highestPlayerOfMatchEachSeason;
