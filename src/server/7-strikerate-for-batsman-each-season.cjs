// Find the strike rate of a batsman for each season

function strikerateForBatsmanEachSeason(matches, deliveries) {
  if (
    matches === undefined ||
    Array.isArray(matches) == false ||
    deliveries === undefined
  ) {
    return {};
  }

  // dictonary for id and season
  const getSeasonbyId = matches.reduce((accumulator, match) => {
    if (match["id"] !== undefined && match["season"] !== undefined) {
      accumulator[match["id"]] = match["season"];
    }
    return accumulator;
  }, {});

  let batsmanStats = deliveries.reduce((accumulator, delivery) => {
    const seasonIPL = getSeasonbyId[delivery["match_id"]];
    if (delivery["batsman"] in accumulator == false) {
      accumulator[delivery["batsman"]] = {};
    }
    if (seasonIPL in accumulator[delivery["batsman"]] == false) {
      accumulator[delivery["batsman"]][seasonIPL] = {
        runs_scored: 0,
        total_balls_played: 0,
        strike_rate: 0,
      };
    }
    accumulator[delivery["batsman"]][seasonIPL]["runs_scored"] =
      accumulator[delivery["batsman"]][seasonIPL]["runs_scored"] +
      Number(delivery["batsman_runs"]);

    accumulator[delivery["batsman"]][seasonIPL]["total_balls_played"]++;

    return accumulator;
  }, {});

  let result = Object.entries(batsmanStats).map((playerEntry) => {
    let seasonList = Object.entries(playerEntry[1]).map((season) => {
      //playerentry[1] holds the object with player stats each season as fields
      season[1]["strike_rate"] =
        (season[1]["runs_scored"] * 100) / season[1]["total_balls_played"];
      return season;
    });

    return [playerEntry[0], Object.fromEntries(seasonList)];
  });
  result = Object.fromEntries(result);

  return result;
}

module.exports = strikerateForBatsmanEachSeason;
