// Number of matches won per team per year in IPL.

function matchesWonPerYearPerTeam(matches) {
  let result = {};
  if (matches === undefined || Array.isArray(matches) == false) {
    return {};
  }

  result = matches.reduce((accumulator, match) => {
    if (
      match["season"] !== undefined &&
      match["team1"] !== undefined &&
      match["team2"] !== undefined &&
      match["winner"] !== undefined
    ) {
      if (match["winner"] in accumulator) {
        if (match["season"] in accumulator[match["winner"]]) {
          accumulator[match["winner"]][match["season"]] += 1;
        } else {
          accumulator[match["winner"]][match["season"]] = 1;
        }
      } else {
        accumulator[match["winner"]] = {};
        accumulator[match["winner"]][match["season"]] = 1;
      }

      if (match["team1"] === match["winner"]) {
        if (match["team2"] in accumulator === false) {
          accumulator[match["team2"]] = {};
          accumulator[match["team2"]][match["season"]] = 0;
        }
      } else if (match["team2"] === match["winner"]) {
        if (match["team1"] in accumulator === false) {
          accumulator[match["team1"]] = {};
          accumulator[match["team1"]][match["season"]] = 0;
        }
      }
    }
    return accumulator;
  }, {});
  return result;
}

module.exports = matchesWonPerYearPerTeam;
