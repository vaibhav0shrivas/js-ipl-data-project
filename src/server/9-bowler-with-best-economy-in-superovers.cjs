// Find the bowler with the best economy in super overs

function economicBowlersinSuperOvers(deliveries) {
  if (deliveries === undefined || Array.isArray(deliveries) == false) {
    return {};
  }

  //economy = (runs_conceded*6)/(total_balls)
  //Byes and leg byes are not charged to the bowler's analysis,//ignore this
  //  and so do not harm their economy rate. On the other hand,
  // the bowler is penalised for wides and no-balls, //add to runs
  // though neither adds a ball to the over.   //dont add to balls
  let bowlerStats = deliveries.reduce((accumulator, delivery) => {
    if (delivery["is_super_over"] == 1) {
      if (delivery["bowler"] in accumulator == false) {
        accumulator[delivery["bowler"]] = {
          runs_conceded: 0,
          total_balls_thrown: 0,
          economy: 0,
        };
      }
      accumulator[delivery["bowler"]]["runs_conceded"] =
        accumulator[delivery["bowler"]]["runs_conceded"] +
        Number(delivery["total_runs"]) -
        Number(delivery["bye_runs"]) -
        Number(delivery["legbye_runs"]);

      accumulator[delivery["bowler"]]["total_balls_thrown"]++;
    }
    return accumulator;
  }, {});

  let playerEconmies = Object.entries(bowlerStats)
    .map((playerentry) => {
      //playerentry[1] holds the object with player stats as fields
      playerentry[1]["economy"] =
        (playerentry[1]["runs_conceded"] * 6) /
        playerentry[1]["total_balls_thrown"];
      return playerentry;
    })
    .sort((playerentryA, playerentryB) => {
      if (playerentryA[1]["economy"] > playerentryB[1]["economy"]) {
        return 1;
      } else {
        return -1;
      }
    });

  let top = playerEconmies[0];
  return top;
}

module.exports = economicBowlersinSuperOvers;
