const fs = require("fs");
const papaParse = require("papaparse");
const path = require("path");
const express = require('express');

const matchesPerYear = require("./src/server/1-matches-per-year.cjs");
const matchesWonPerYearPerTeam = require("./src/server/2-matches-won-per-team-per-year.cjs");
const extraRunsPerTeaminYear = require("./src/server/3-extra-runs-conceded-per-team.cjs");
const economicBowlersinYear = require("./src/server/4-top-10-economic-bowlers-in-year.cjs");
const teamWonTossAndGame = require("./src/server/5-team-won-toss-and-game.cjs");
const highestPlayerOfMatchEachSeason = require("./src/server/6-highest-player-of-the-match-each-season.cjs");
const strikerateForBatsmanEachSeason = require("./src/server/7-strikerate-for-batsman-each-season.cjs");
const highestDismisal = require("./src/server/8-highest-dismisal-one-player-by-another.cjs");
const bestBowlerInSuperOvers = require("./src/server/9-bowler-with-best-economy-in-superovers.cjs");

const deliveriesDataPath = path.join(__dirname, "src/data/deliveries.csv");
const outputFolderPath = path.join(__dirname, "/src/public/output/");
const matchesDataPath = path.join(__dirname, "src/data/matches.csv");
const pathForHtml = path.join(__dirname, "/static/index.html");
const pathForCSS = path.join(__dirname, "/static/style.css");
const pathForlogo = path.join(__dirname, "/static/assets/ipl-logo.png");

let matches = {};
let deliveries = {};

function makeOutputFiles() {
  try {
    const data = fs.readFileSync(matchesDataPath);
    matches = papaParse.parse(data.toString(), {
      header: true,
    }).data;
  } catch (err) {
    console.error("while reading and parsing matches.csv file =>", err);
  }

  try {
    const data = fs.readFileSync(deliveriesDataPath);
    deliveries = papaParse.parse(data.toString(), {
      header: true,
    }).data;
  } catch (err) {
    console.error("while reading and parsing deliveries.csv file =>", err);
  }

  const resultOfMatchesPerYear = matchesPerYear(matches);

  fs.writeFile(
    path.join(outputFolderPath, "1-matches-per-year.json"),
    JSON.stringify(resultOfMatchesPerYear),
    (err) => {
      if (err) {
        console.error("while writing output 1, error occured =>", err);
      }
    }
  );

  const resultOfMatchesWonPerYearPerTeam = matchesWonPerYearPerTeam(matches);

  fs.writeFile(
    path.join(outputFolderPath, "2-matches-won-per-team-per-year.json"),
    JSON.stringify(resultOfMatchesWonPerYearPerTeam),
    (err) => {
      if (err) {
        console.error("while writing output 2, error occured =>", err);
      }
    }
  );

  const resultOfExtraRunsPerTeaminYear = extraRunsPerTeaminYear(matches, deliveries, 2016);

  fs.writeFile(
    path.join(outputFolderPath, "3-extra-runs-conceded-per-team.json"),
    JSON.stringify(resultOfExtraRunsPerTeaminYear),
    (err) => {
      if (err) {
        console.error("while writing output 3, error occured =>", err);
      }
    }
  );

  const resultOfEconomicBowlersinYear = economicBowlersinYear(matches, deliveries, 2015);

  fs.writeFile(
    path.join(outputFolderPath, "4-top-10-economic-bowlers-in-year.json"),
    JSON.stringify(resultOfEconomicBowlersinYear),
    (err) => {
      if (err) {
        console.error("while writing output 4, error occured =>", err);
      }
    }
  );

  const resultOfTeamWonTossAndGame = teamWonTossAndGame(matches);

  fs.writeFile(
    path.join(outputFolderPath, "5-team-won-toss-and-game.json"),
    JSON.stringify(resultOfTeamWonTossAndGame),
    (err) => {
      if (err) {
        console.error("while writing output 5, error occured =>", err);
      }
    }
  );

  const resultOfHighestPlayerOfMatchEachSeason = highestPlayerOfMatchEachSeason(matches);

  fs.writeFile(
    path.join(outputFolderPath, "6-highest-player-of-the-match-each-season.json"),
    JSON.stringify(resultOfHighestPlayerOfMatchEachSeason),
    (err) => {
      if (err) {
        console.error("while writing output 6, error occured =>", err);
      }
    }
  );

  const resultOfStrikerate = strikerateForBatsmanEachSeason(matches, deliveries);

  fs.writeFile(
    path.join(outputFolderPath, "7-strikerate-for-batsman-each-season.json"),
    JSON.stringify(resultOfStrikerate),
    (err) => {
      if (err) {
        console.error("while writing output 7, error occured =>", err);
      }
    }
  );

  const resultOfHighestDismisal = highestDismisal(deliveries);

  fs.writeFile(
    path.join(outputFolderPath, "8-highest-dismisal-one-player-by-another.json"),
    JSON.stringify(resultOfHighestDismisal),
    (err) => {
      if (err) {
        console.error("while writing output 8, error occured =>", err);
      }
    }
  );

  const resultOfbestBowlerInSuperOvers = bestBowlerInSuperOvers(deliveries);

  fs.writeFile(
    path.join(outputFolderPath, "9-bowler-with-best-economy-in-superovers.json"),
    JSON.stringify(resultOfbestBowlerInSuperOvers),
    (err) => {
      if (err) {
        console.error("while writing output 9, error occured =>", err);
      }
    }
  );

}

const readFromFile = function (fileToBeRead, options) {
  options = Object.assign(
    {
      encoding: "utf-8",
    },
    options
  );

  return new Promise((resolve, rejects) => {

    fs.readFile(fileToBeRead, options.encoding, (error, dataFromFile) => {
      if (error) {

        console.error(`Failed to read ${fileToBeRead}.`);
        rejects(error);
      } else {

        console.log(`Read ${fileToBeRead} sucessfully.`);
        resolve(dataFromFile);
      }
    });
  });
};

const app = express();
const port = process.env.PORT || 3000;

makeOutputFiles();

app.use(express.static(path.join(__dirname, "/static")));

app.get('/', (req, res) => {
  res.sendFile(pathForHtml);
});



app.get('/:name', (req, res) => {
  const fileName = req.params.name;
  const pathForResponseFile = path.join(outputFolderPath, `${fileName}.json`);
  readFromFile(pathForResponseFile)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      console.log(err);
      res.send("Sorry Data Unavailable!");

    });

});

app.listen(port, () => {
  console.log(`IPL-data-project Listening on ${port}`);
});